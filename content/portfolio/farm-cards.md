---
title: 'Open Source Farm Cards Assessments'
description: 'An overview of agricultural assessment procedures in Illinois, and how to accomplish them using spatial Python.'
draft: false
toc: true
tags:
    - python
    - kendall county
date: 15 Oct 2023
---

## Overview

*See the corresponding presentation slides [here](/talks/farm-cards/index.qmd), as well as the code repository [here](https://codeberg.org/kendall-county-gis/farm-cards)

My main goals in this project have been:

- replace and improve upon legacy workflow
- use open-source tools
- document the process to make it accessible to others

The intended audience of this work is other county assessment / GIS staff in Illinois, though certainly aspects of this could apply to other things.

## What are Farm Cards?

At Kendall County (and perhaps elsewhere), we refer to agricultural assessments as "farm cards". The term is a holdover from an analog age in which agricultural assessments were tabulated on a literal card.

![](/media/farm-cards/sample_card.png)

For large, agricultural parcels, the value of a given parcel is determined by its **Productivity Index**, which is (mostly) calculated from:

1. The soil type
1. The slope
1. How the land is being used

For those interested in the full details, you can real the Illinois Department of Revenue's [Publication 122](https://tax.illinois.gov/content/dam/soi/en/web/tax/research/publications/pubs/documents/pub-122.pdf).

At its heart, this is a *spatial overlay*, which is how it came to be the task of the County GIS department in the first place.

### The Individual Soil Weighting Method

Following the IDOR's guidelines, we follow a 10-step procedure:

1. Get parcel boundaries and aerial imagery
1. Overlay soil types
1. Using imagery and on-site inspections, classify landuse areas
1. Determine acreage of each soil type per landuse category
1. Determine PI (productivity index) ratings for soil types
1. Adjust PI values based on slope
1. Determine Equalized Assessed Value per acre using IDOR reference table
1. Calculate assessed value by multiplying by acres
1. Subtotal acres / values for each soil/slope/landuse combination
1. Sum by parcel for full assessed value

## Setting up our Environment

*Note*: Some of this section will be quite basic if you're already familiar with spatial Python, and you may wish to skip to the next section.

### Data Manipulation and Spatial Overlays
`geopandas`, a spatial extension of `pandas`, is the backbone of this process.

```python
import geopandas as gp
import pandas as pd
import numpy as np
```

### Other Modules
```python
# requesting data from the web
import requests

# plotting figures
import matplotlib.pyplot as plt

# working with files
import os
from pathlib import Path

# working with time
from datetime import datetime
```

### Parameters
```python
#| tags: [parameters]

# Spatial reference for layers
sr = "{'wkid': 3435}"

# Parcels REST service
parcels_url = 'https://maps.co.kendall.il.us/server/rest/services/Hosted/Current_Cadastral_Features/FeatureServer/1/query?'

# SSURGO Soils REST service
soils_url = 'https://maps.co.kendall.il.us/server/rest/services/Hosted/Assessor_Soils/FeatureServer/0/query?'

# Landuse REST service
landuse_url = 'https://maps.co.kendall.il.us/server/rest/services/Hosted/Assessor_Landuse/FeatureServer/0/query?'

# Prepare output, remove accidental duplicates from repeated runs
out_dir = os.path.expanduser('~/')
export_name = f"farms_{datetime.now().strftime('%Y%m%d-%H%M')}"
file = os.path.join(out_dir, f'{export_name}.txt')

if os.path.exists(file):
    print('Output file already exists. Removing!')
    os.remove(file)
print('Good to go!')
```

## Getting the Data

### Parcels

Here we will use the parcels data to demonstrate several methods of bringing spatial data into the project. Kendall County's data currently comes through ArcGIS REST endpoints, but the process can easily adapt.

#### ArcGIS Feature Service

As long as the feature service is public, no need to mess with any fancy modules. `requests` will be all we need here.

```python
# Define parameters
parcels_params = {
    'where': "pin LIKE '05-04-431-00%'",
    'outFields': 'gross_acres, pin',
    'outSR': sr,
    'f': 'geojson'
}

# Submit request and read response into a GeoDataFrame
parcels = requests.get(parcels_url, parcels_params)

p_df = gp.read_file(parcels.text)
p_df.plot(column='pin')
```

![](/media/farm-cards/farm-card-plot.png)

#### From a File

GeoPandas can handle pretty much anything you throw at it. For database-like types such as GeoPackage and Esri GeoDatabase, simply specify the `layer` as an additional parameter.

```python
# GeoJSON
gp.read_file(Path('./input_pins.geojson').resolve()).sample()
```

#### From a Database

Depending on the database, this will require some other adaptor module.

##### PostGIS

GeoPandas is also great in that it can query a PostGIS database directly.

```python
postgres_conn = 'postgresql://user:pw@host:port/database'

postgres_query = 'select * from parcels'

gp.read_postgis(postgres_query, postgres_conn)
```

##### Something Else?

Maybe GeoPandas can't read other databases the same way, but Pandas can! All you need to do is query it first with Pandas, then use the `GeoDataFrame` constructor to bring the results into GeoPandas.

```python
sql_conn = 'some connection string'
sql_query = 'some query or table name'

df = pd.read_sql(sql_query, sql_conn)

gp.GeoDataFrame(df, geometry=df['geom_column'], crs='your CRS')
```

### Soils and Landuse

#### Spatial Filter

Unlike our parcels, we don't have a list of unique identifiers to restrict our query. We'd like to avoid bringing in the entire layer for performance reasons, so how do we do this?

Since we are working with ArcGIS Feature Services, we can include a spatial filter in our query, and we can use our parcels to determine what that filter should be like.

Note the output of our parcel dataframe's `total_bounds` attribute.

```python
p_df.total_bounds
```

For the REST endpoint, the **esriGeometryEnvelope** we create needs to follow the format of a *string*, `min_x, min_y, max_x, max_y`. A little list comprehension is all we need.

```python
bbox = ','.join([str(i) for i in p_df.total_bounds])
bbox
```

And do note here that if you're using `gp.read_file`, it accepts the parameter `bbox` as a *tuple*. For databases, you can build an intersecting shape and use something akin to PostGIS's `ST_INTERSECTS`.

#### Constructing Queries

The queries for both soils and landuse layers are the same:

```python
farm_params = {
    'where': '1=1',
    'outFields': '*',
    'returnGeometry': True,
    'geometryType': 'esriGeometryEnvelope',
    'geometry': bbox,
    'spatialRel': 'esriSpatialRelIntersects',
    'outSR': sr,
    'f': 'geojson'
}

landuse = requests.get(landuse_url, farm_params)
l_df = gp.read_file(landuse.text)

soils = requests.get(soils_url, farm_params)
s_df = gp.read_file(soils.text)
s_df.sample()
```

A few kinds of soil types (like `W`, which is water) have no slope code. This will totally mess up our aggregation later on, so we need to fill these!

```python
s_df.loc[:, 'slope'].fillna('', inplace=True)
```

### Visualization

Before moving on, we will plot all our frames at the same extent, to show that all required features have been successfully retrieved.

![](/media/farm-cards/farm-overlay-plot.png)

## Data Manipulation

### Adding Calculated Area

Before we actually split the parcels up, we have to address an issue. Assessed values are based on *deeded* acreage, which may not be the same as the measured acreage. Once the parcel is cut in pieces, we need to be able to say what portion of the deeded acreage that part is "worth".

In order to accurately calculate our farm cards, we'll be using the calculated area of the geometry in the given spatial reference. *Deeded* acreage trumps *actual* acreage, though, so we need to be able to scale our calculations accordingly.

To do this, we'll create a new column in the dataframe, calc_area. GeoDataFrames have a built-in property `area`, which returns the area of the shape in the given spatial reference. Later, we can calculate the area of each *part*, which will give us the ratio we need in order to scale the deeded value. As the units cancel out, we don't need to worry that our measured values are not acres.

```goat
Deeded Acreage of Part      Measured Sq. Ft of Part
-----------------------  =  ------------------------
Deeded Acreage of Whole     Measured Sq. Ft of Whole

                         |
                         v
                                                        Measured Sq. Ft of Part
Deeded Acreage of Part   =  Deeded Acreage of Whole  X  ------------------------
                                                        Measured Sq. Ft of Whole
```

```python
p_df['calc_area'] = p_df.area
p_df.sample()
```

### Overlay Operations

GeoPandas, again, has exactly what we want: `overlay`.

#### Warnings

Our features don't always line up perfectly, and running an intersection can result in the creation of lines and points. GeoPandas will drop other types, and warns us when it does so. Running this against a large number of parcels can mean a *lot* of warnings clogging our output, so we will just filter out the specific message.

```python
import warnings
warnings.filterwarnings('ignore', '.*dropped geometries of different geometry types.*')
```

#### Perform Overlay

```python
df = gp.overlay(p_df, s_df, how='intersection')
df = gp.overlay(df, l_df, how='intersection')
```

### Removing Extra Fields

For our calculations, we only need a few fields from the output. We'll define our "keepers", then remove everything else.

```python
keepers = [
    'gross_acres',
    'gis_acres',
    'calc_area',
    'pin',
    'soil_type',
    'slope',
    'landuse_type',
    'geometry'
]

df.drop(columns=[c for c in df if c not in keepers], inplace=True)

df.sample()
```

### Calculating Part Area

First, we'll show each step broken out:
1. Calculate area of parts
1. Calculate part / whole ratio
1. Multiply deeded acreage of whole by ratio

```python
df.assign(
    part_area = df.area,
    area_ratio = lambda x: x['part_area'] / x['calc_area'],
    part_acres = lambda x: x['area_ratio'] * x['gross_acres']
)[['pin', 'calc_area', 'part_area', 'area_ratio', 'gross_acres', 'part_acres']].head()
```

Doing it all at once, then showing the result:

```python
df['part_acres'] = df.area / df['calc_area'] * df['gross_acres']

df.plot(column='part_acres', edgecolor="black")
```

![](/media/farm-cards/overlay.png)

## Validating Results

### Checking Acreage Sums

So far, it's been as simple as I'd like. But is it **accurate**? The legacy tool being replaced had produced differences as large as tenths of acres; if this doesn't do better, it's a waste of time.

We'll use `groupby` and `agg` to compare our acreage values. Our assessments databases recods acreage out to 4 decimal places, so we'll round things to the same.

```python
qc = df.groupby('pin').agg({'gross_acres':'max', 'part_acres':'sum'}).reset_index()
qc['diff'] = qc['gross_acres'] - round(qc['part_acres'], 4)

qc.head()
```

|pin|gross_acres|part_acres|diff|
|-|-|-|-|
|05-04-431-002|13.2001|13.199523|0.0006|

All of this has just been **math**. If there is a measurable difference here, it is because of a gap or overlap in the input data. Soil data comes from [SSURGO](https://data.nal.usda.gov/dataset/soil-survey-geographic-database-ssurgo), and is known to be good. So we want to look at our landuse and parcels to identify potential issues.

```python
p_df.query("pin == '05-04-431-002'").overlay(l_df, 'symmetric_difference').plot(column='pin', edgecolor='black')
```

![](/media/farm-cards/sym_diff.png)

## Tidy Up and Export

For Kendall County GIS, this is as far as we go. The remainder of the process is handled by our assessments software, so we need to get this information in an importable format.

This section is even more specific to Kendall County than the rest, but could easily be adapted for other use cases.

### Fields

For our export, some of the fields can be dropped entirely. There are also a few formatting particularities to address.

```python
df.drop(columns=['gross_acres', 'calc_area', 'geometry'], inplace=True)

df.loc[:,'pin'] = df['pin'].str.replace('-', '') # remove hyphens from pin
df.loc[:, 'landuse_type'] = '0' + df['landuse_type'].astype('str') # change landuse to string with leading 0

df.sample()
```

### Aggregating Data

Our overlays returned *singlepart* geometries, so the dataframe may have rows in which the only difference is the acreage. For valuation, we just need to *total* acreage per soil/landuse/slope combination, so we can aggregate those values.

It's not strictly necessary, but makes for a more concise output.

```python
out_cols = ['pin', 'soil_type', 'slope', 'landuse_type']

aggs = {
    'soil_type':'first',
    'slope':'first',
    'landuse_type':'first',
    'pin':'first',
    'part_acres':'sum'
}

agg_df = df.groupby(by=out_cols, as_index=False).agg(aggs).reset_index(drop=True)

agg_df.head()
```

### Rounding and Filtering

As noted above, anything beyond 4 decimal places is too precise for our assessment database. Our overlays can create *tiny* features, which, when imported, round off to `0.0000` acres. These rows can be dropped from the output entirely.

```python
agg_df.loc[:, 'part_acres'] = round(agg_df.loc[:, 'part_acres'], 4)

agg_df = agg_df[agg_df.loc[:, 'part_acres'] > 0]
```

### Export to File

For Kendall County, this is the finish line. In spite of its name, Pandas' `to_csv()` method can write to any file type we like.

```python
agg_df.to_csv(file, sep='\t', header=False)

with open(file, 'r') as output:
    print(output.read())
```

## Valuation

Not content to leave the rest of this process in a proprietary black box, I wanted to see if the valuation could be handled here, too. Of course, the answer is **yes**.

### Additional Tables

In order to calculate the assessment values, you'll need a few other datasets.

- Soil Productivity Index
- Slope and Erosion Adjustments
- Equalized Assessed Value / PI Table

These can all be found in Publication 122, but are, unhelpfully, in PDF. For the sake of this demonstration, I have copied these tables into CSV files.

We can load each of them as their own dataframes. Lacking any spatial component, we can use Pandas for this.

#### Productivity Index

```python
pi_df = pd.read_csv(Path('soil_PI.csv').resolve())
pi_df.head()
```

#### Slope and Erosion Coefficients

While a full table of coefficients exists for slopes 0 - 43%, we don't actually have slope data in that detail. Our soil data comes with slope / erosion **letters**, which decode as follows:

|Code    |%Slope |
|--------|-------|
|None / A|00 - 02|
|B       |02 - 05|
|C       |05 - 10|
|D       |10 - 15|
|E       |15 - 18|
|F       |18 - 35|
|G       |35 - 70|

Per Pub 122:

> Because Table 3 cannot be used with slope ranges, use a central point of the slope ranges unless a better determinant of slope is available.

For our case, we'll use a central value of each class to determine the coefficient to apply to the PI.

Within each slope class, the erosion number further modifies the coefficient, and not consistently. That is, moderate erosion on flat terrain will impact productivity less than moderate erosion on steep terrain.

```python
se_df = pd.read_csv(Path('soil_slope_erosion.csv').resolve())
se_df.head()
```

### EAV

This table gets a special mention. Table 1 from Pub 122 contains the certified per-acre EAV for a given productivity index.

And don't be fooled by table 1's headers! Even though the second to last column is titled Equalized Assessed Value (something you might shorten to, y'know, E-A-V), whenever Publication 122 makes reference to an EAV in their instructions, they are referring to the *Certified Value*.

The lowest certified PI is 82, but after applying slope and erosion coefficients, it is **not** the lowest possible PI value.

IDOR provides a [guidance document](https://tax.illinois.gov/content/dam/soi/en/web/tax/localgovernments/property/documents/pibelow82.pdf) on calculating EAV for a PI below 82.

Of the two calculations below, we take whichever produces a greater value at a PI of `n`.

> EAV_n = EAV_min - ((EAV_min+5 - EAV_min) / 5 * (min - n))

> EAV_n = EAV_min / 3

With that equation handy, we can create a dataframe of our sub-82 values and append it to the official table.

```python
eav_df = pd.read_csv(Path('eav_2022.csv').resolve())

sub82 = pd.DataFrame({'avg_PI': np.arange(1,82)})
sub82['eav'] = 199.29 - (((207.47-199.29)/5)*(82-sub82['avg_PI']))

eav = pd.concat([eav_df, sub82])
```

### Merge DataFrames

Now we have our dataframes with overlapping columns. We can use the `merge` method to do a database-style join between them.

```python
val_df = agg_df.merge(pi_df, how='left', left_on='soil_type', right_on='map_symbol', suffixes=('','_desc'))
val_df = val_df.merge(se_df, how='left', left_on='slope', right_on='erosion_code')

val_df.head()
```

### Adjusted PI

To get the adjusted `PI`, we multiply the value from the soils table with the coefficients, but we do so based on the *favorability* of the soil type.

Using NumPy's `where` method, we can easily apply different calculations on an either/or basis.

```python
val_df.insert(
    5,
    'adj_PI',
    np.where(
        val_df['favorability'] == 'Favorable',
        val_df['productivity_index'] * val_df['coeff_fav'],
        val_df['productivity_index'] * val_df['coeff_unf']
    )
)

val_df.head()
```

To merge the EAV in, we will need our `PI` as an *integer*.

```python
val_df.loc[:,'adj_PI'] = val_df.loc[:,'adj_PI'].round()
val_df = val_df.merge(eav_df, how='left', left_on='adj_PI', right_on='avg_PI')

val_df.head()
```

### Apply Landuse Coefficients

Landuse is simple. For the big three landuse types, cropland, pasture, and other farmland, it's basic division. Nearly everything else is zeroed out (hydro, roadways), or assessed separately (homesites).

|Landuse                |Calculation    |
|-----------------------|---------------|
|Cropland               |`EAV`          |
|Permanent Pasture      |`EAV` / 3      |
|Other Farmland         |`EAV` / 6      |
|Contributory Wasteland |33.22          |

#### Create and Populate New Field

```python
val_df.insert(
    6,
    'eav_adj',
    np.where(
        val_df['landuse_type'] == '02',
        val_df['eav'],
        0
    )
)

# permanent pasture
val_df.loc[val_df['landuse_type']=='03', 'eav_adj'] = val_df['eav']/3

# other farmland
val_df.loc[val_df['landuse_type']=='04', 'eav_adj'] = val_df['eav']/6

# contributory wasteland
val_df.loc[val_df['landuse_type'] == '05', 'eav_adj'] = 33.22

val_df[['landuse_type', 'eav', 'eav_adj']].sample(5)
```

### Multiply Adjusted EAV by Acres

```python
val_df['value'] = val_df['part_acres'] * val_df['eav_adj']

val_df[['pin', 'soil_type', 'slope', 'landuse_type', 'part_acres', 'value']].fillna(0).sample(5)
```

That's it! We've taken our farm calculations from start to finish, and have real dollar amounts.

## Extending Things

There are lots of ways to develop this idea further.

- Create a standalone function that can take lists / files of parcel numbers as parameters
- Wrap the process in a GUI to make it accessible to other staff
- Put it in a cloud environment like AWS Lambda or Azure Functions and trigger through an API
- Generate per-parcel PDFs with maps and tables

If any of this interests you or could apply to your work, by all means, go into [the code repository](https://codeberg.org/kendall-county-gis/farm-cards) and copy what you like.