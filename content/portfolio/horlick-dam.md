---
title: Modeling Seasonal Flow and Depth of the Root River without the Horlick Dam
toc: true
tags:
    - uw-parkside
    - student work
    - hydrology
date: 2018-04-01
---

## Background

As of 2014, the Wisconsin DNR has designated the Horlick Dam as a Significant Hazard. This designation means that there is a high  likelihood for the dam to fail in future 100-year flood events, as the  dam in its current state is only rated to withstand a 10-year flood. Racine County has until 2024 to address this issue.[^1]

Though a decision has yet to be finalized, county and regional officials have favored full removal of the dam as the most cost- effective and most beneficial long-term solution.

Area residents, many of whom purchased their homes for their adjacency to the dam impoundment, have opposed the potential removal, though their grounds for opposition are primarily based upon loss of recreational value.

In response to these concerns, the Southeastern Wisconsin Regional Planning Commission prepared several documents to educate the public as to the likely future course of the river.

## Need for New Analysis

The maps currently provided regarding this issue do not adequately address the recreational impacts to area residents. The maps only reflect changes to the river’s width, despite depth being the crucial variable to water recreation.[^2]

Additionally, in 2016 the Great Lakes Compact approved Waukesha’s application to draw Lake Michigan water, on the condition that their treated wastewater be discharged back to the Lake Michigan basin via the Root River. Per the agreement, Waukesha will be allowed up to 8.2 million gallons per day, or about 13 cubic feet per second.[^3]

This discharge will significantly effect the baseline flow of the Root River, especially during periods of low flow, and earlier analyses do not reflect this.

## Objective

In this project, I intend to re-run similar modeling to SEWRPC’s analysis while including additional flow data from Waukesha’s waste water discharge.

From the output, I plan to create maps which demonstrate the seasonal range of water levels, and the resulting depths of the river. Assuming a certain minimum depth of water required to pilot a kayak or canoe, the final maps should indicate to residents more accurately if and when they will be able to use the river recreationally. 

## Data Sources

The following data sources were used to create the model

- HEC-2  One dimensional flow data from 1990 and 2000 floodwater surveys of Root River
- National Flood Hazard Layer
- 5-ft DEM for Racine County
- 53 years of USGS Gage Data from stations on Root River
- National Land Cover Dataset
- SEWRPC Horlick Dam Impoundment Sediment Survey

## Methods

HEC (Hydrologic Engineering Center) survey and sediment core data were obtained from the Southeastern Wisconsin Regional Planning Commission. Core location and river cross-section data were not initially georeferenced.

![](/media/horlick/hecras-transect.jpg)

Data from the HEC survey were used to develop FEMA’s National Flood Hazard Layer, which was available as a shapefile. Cross-section vertices were extracted and copied into HEC’s RAS (River Analysis Software).

![](/media/horlick/hecras-transect-selected.jpg)

A map of sediment core locations was obtained from SEWRPC, and location coordinates were likewise extracted and copied into RAS. As sediment cores were not part of the NFHL dataset, additional cross-section lines were created. Racine County’s 5-ft DEM was loaded into RAS as the terrain layer; cross section points could then be compared against elevation profiles to check for accuracy.

Once cross section points were correctly placed, channel elevation was interpolated using the Triangular Interpolation method. Given the sparseness of the points, TIN was chosen to ensure that the minimum and maximum values along the channel were not exceeded between individual cross-sections. The resulting raster was filtered using Raster Calculator to only include cells below the elevation of the original water level, 630 ft. The filtered raster was then merged with the DEM to create a single raster layer containing elevation information for the terrain as well as the river channel.

![](/media/horlick/tinmap.jpg)

![](/media/horlick/hecras-isometric.jpg)

Information from the National Land Cover Dataset was imported into RAS and used to obtain approximate N (Manning’s Roughness) values. Manning’s N values are used to calculate how much drag a given surface will generate as water flows over it, which can affect velocity.

In order to run a simulation in RAS, flow rate needed to be established. To determine flow, historic data from USGS gage stations along the Root River were collected. Gage readings were separated into week number cohorts and analyzed separately to determine seasonal variation. From these results, it was possible to identify seasonal fluctuations, and to pull the inner quartile range for seasonal high and low flow periods.

Flow rates were then entered into RAS. Using RAS’s built-in Steady Flow Analysis, multiple flow rates were simulated. The resulting output indicated water level, and by overlaying the results onto the aforementioned merged raster, could indicate water depth and bank lines.

![](/media/horlick/seasonalflux.jpg)

## Conclusion

Examining the projected depth of the river in light of pending infrastructure changes, it is clear that while the nature of the river will be altered, area residents will still be able to enjoy boating on the Root River.

As water levels drop below 623 feet in elevation, areas less than two feet of depth become more common at the north end of the current impoundment, and passage in a two-person canoe may become difficult. However, this water level occurs below a threshold flow rate of 20 CFS, which based on gage data analysis will only occur less than 10% of the time.

Boating on the Root will perhaps require more effort in the future, as it will no longer be a pond-like impoundment, but it will still be accessible more often than not.

![](/media/horlick/seasonal-bounds.jpg)

[^1]: Wisconsin Department of Natural Resources. Dam Inspection Report: Detailed Information For Dam Horlicks. Madison, 2014.
[^2]: Southeastern Wisconsin Regional Planning Commission. Community Assistance Planning Report No. 316: A Restoration Plan For The Root River Watershed. Waukesha, 2014.
[^3]: Great Lakes - St. Lawrence River Basin Water Resources Council. No. 2016-1: In The Matter Of The Application By The City Of Waukesha, Wisconsin For A Diversion Of Great Lakes Water From Lake Michigan And An Exception To Allow The Diversion. Chicago, 2016.
