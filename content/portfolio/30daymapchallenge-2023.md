---
title: "30 Day Map Challenge 2023"
draft: false
toc: true
description: "Another year of making maps"
date: 30 Nov 2023
tags: 
- cartography
- 30daymapchallenge
---

I can't believe it's already here again! Well, here we go. I'll be updating this page sporadically throughout the month, but posting over at [mapstodon.space](https://mapstodon.space) as I finish these.

## Day 1: Points

What points? Signs do! You think of a thing, it's probably in OSM *somewhere*. I found a region in Italy that is positively *loaded* with `information=guidepost` points that had an azimuth bearing in their `direction` tag.

![](/media/30daychallenge-2023/day1.jpg)


## Day 2: Lines

Day 1 was "points that point", so Day 2 had to be Lines that Line. Springfield, IL has a *ton* of their street markings mapped out in OSM, to the point that individual intersections and the broader street network are recognizable.

![](/media/30daychallenge-2023/day2.jpg)


## Day 3: Polygons

I used to live in the City of Racine, and their GIS maintains a pretty impressive "edge of pavement" layer. I cleaned it up a little, ran it through QGIS's "Lines to Polygons" and "Topological Coloring" tools to make this.

![](/media/30daychallenge-2023/day3.jpg)


## Day 4: Bad Map

Grab a random layer from a public AGOL account, don't do anything to make it comprehensible.

![](/media/30daychallenge-2023/day4.jpg)


## Day 5: Analog Map

Jon Nelson did a whole keynote at the 2023 Esri Midwest UC that demonstrated the "handness" of the Midwestern states. Turns out Illinois is a better hand than Michigan! Well, depending on your projection I suppose. Here's my map, with a real map next to it to compare.

![](/media/30daychallenge-2023/day5.jpg)


## Day 10: North America

Messing with Blender a bit. I never have any luck with the GIS plugin on there, so I just brought some rasters in and went for it. Threw them at a nurbs surface, did some shader node stuff, and... pretty good! For a Blender amateur just fooling around, I think it looks good.

![](/media/30daychallenge-2023/day10.jpg)


## Day 11: Retro

Recreating a favorite symbology style from [this blog](https://hannes.enjoys.it/blog/2018/12/fake-chromatic-aberration-and-dynamic-label-shadows-in-qgis/) in QGIS. It's honestly a bit hard to look at for too long, though.

![](/media/30daychallenge-2023/day11.jpg)


## Day 15: OpenStreetMap

I love OSM because of how detailed and niche you can get, given the free-form tagging structure. Pick a hobby, *somebody* has probably mapped it out. In this case, it was me that did the mapping. The hobby: disc golf.

I *love* the "woods" effect I got on this one with some geometry generator and layer effects in QGIS.

![](/media/30daychallenge-2023/day15.jpg)


## Day 20: Outdoors

Or out-doors, in this case. OSM, ever the wealth of edge-case data, has a *lot* of nodes tagged `entrance=exit`. I found one particular building with a lot of them.

![](/media/30daychallenge-2023/day20.jpg)


## Day 22: North Isn't Always Up

This may be my *favorite* post from the challenge. It came out so well!

Using NOAA's dataset of magnetic declination, some QGIS Atlas magic, and a layout with 3 different projections. It was quite a stretch for me, but I feel it was absolutely worth it.

{{<video src="/media/30daychallenge-2023/day22.mp4" type="video/mp4">}}


## Day 27: Dots

Another "browse a random AGOL server" day.

![](/media/30daychallenge-2023/day27.jpg)


## Day 28: Chart or Map?

If you've ever flown over the US, there are *scads* of center-pivot irrigation plots out there. Happened to find one in even thirds. I think I should have added percentage labels to really play up the chart aspect.

![](/media/30daychallenge-2023/day28.jpg)

