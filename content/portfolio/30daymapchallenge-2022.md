---
title: "30 Day Map Challenge 2022"
draft: false
toc: true
description: "2022 was my first year participating in the #30DayMapChallenge, and I had a great time! It was really nice to get out of my comfort zone and just try new things."
tags: 
    - cartography
    - 30daymapchallenge
date: 30 Nov 2022
---

[30 Day Map Challenge](https://30daymapchallenge.com/)

## Day 1: Points
A subjective map of biking hazards.

![](/media/30daychallenge-2022/Day1.jpg)

## Day 2: Lines
Highlighting the weirdness that resulted from the Indian Boundary Line and the I&M Canal.

![](/media/30daychallenge-2022/Day2.jpg)

## Day 3: Polygons
![](/media/30daychallenge-2022/Day3.jpg)

## Day 4: Green
![](/media/30daychallenge-2022/Day4.jpg)

## Day 5: Ukraine
A bit embarrassed by this one. I am not great with my world geography, and while Crimea did cross my mind, I blindly used the Natural Earth political polygons, resulting in the map shown.

![](/media/30daychallenge-2022/Day5.jpg)

## Day 6: Network
![](/media/30daychallenge-2022/Day6.jpg)

## Day 7: Raster
![](/media/30daychallenge-2022/Day7.jpg)

## Day 8: OpenStreetMap
![](/media/30daychallenge-2022/Day8.jpg)
![](/media/30daychallenge-2022/Day8_bonus.jpg)

## Day 9: Space
As in, *living space*. See if you can spot the 2008 recession!

![](/media/30daychallenge-2022/Day9.gif)

## Day 10: Bad Map
Bad color scale for accessibility, too many bins, MAUP. Bad labels, map surrounds that don't actually help.

![](/media/30daychallenge-2022/Day10.jpg)

## Day 11: Red
![](/media/30daychallenge-2022/Day11.jpg)

## Day 12: Scale
![](/media/30daychallenge-2022/Day12.jpg)

## Day 13: 5 Minute Map
![](/media/30daychallenge-2022/Day13.jpg)

## Day 14: Hexagons
As good an excuse as any to mess around with Blender.

![](/media/30daychallenge-2022/Day14.jpg)

## Day 15: Food & Drink
Title is taken from a Dave Eggers book **The Every**, referring to the shame felt by eating a banana when there is so much produce you could get locally. I looked at all the foods I ate in a day and tried to figure out where they came from.

![](/media/30daychallenge-2022/Day15.jpg)

## Day 16: Minimal
![](/media/30daychallenge-2022/Day16.jpg)

~~## Day 17: Map w/o Computer~~

## Day 18: Blue
![](/media/30daychallenge-2022/Day18.jpg)

## Day 19: Globe
Another attempt at Blender. It's not very good! I was inspired by a globe I saw at the Robinson Map Library at UW-Milwaukee once.

![](/media/30daychallenge-2022/Day19.gif)

## Day 20: My Favorite
I went with my favorite dataset, OpenStreetMap, and rendered literally everything.

![](/media/30daychallenge-2022/Day20.jpg)

## Day 21: Kontur Data
![](/media/30daychallenge-2022/Day21.jpg)

## Day 22: NULL
![](/media/30daychallenge-2022/Day22.jpg)

## Day 23: Movement
This one was sort of culmination of other ideas from the challenge (hexagon, network, minimal), and something I have thought about before. When I go to a forest preserve, vector data over aerial imagery is usually not very helpful to me.
In fact, highly detailed maps in *general* are not very useful to me. When I'm walking the trails, I want to see what my current trail connects to, and maybe a really rough sense of distance. This "Trail Card" is my attempt to present that information in a way that could fit on a small embossed piece of cardstock, printed with a single color.

Maybe the Forest Preserve will actually go for it someday. For now, it's a fun idea.

![](/media/30daychallenge-2022/Day23.png)

---

## The Rest

After Thanksgiving, I sort of lost momentum. Maybe next year I'll get all 30!