---
title: '30 Day Map Challenge 2024'
date: 2024-11-30T22:12:48-05:00
toc: true
---

Well, another year has come and gone. I should make more of a habit posting maps here throughout the rest of the year, honestly. November is typically the busiest month at the County, and I can't make but a meager showing during the challenge. But I have a few maps, all the same:

## Day 1: Points

Quick and rough, more an excuse to play with [Andy Woodruff's style ideas](https://andywoodruff.com/posts/2024/qgis-hand-drawn-maps/) than anything else.

Though the map is full of areas, the symbology is entirely point pattern fills, with a variety of randomization, geometry generator and blur effects added.

![](/media/30daychallenge-2024/day1-1.jpg)

The initial result just feels like I forgot my glasses. I think when *this much* blurring is going on, there needs to be at least a few crisp features in there to give some visual contrast. But I **love** the tree area, and will be re-using that symbol style in the future.

## Day 2: Lines

More messing around with random styles. I'd made a map in this style before, but wanted to recreate it so I could remember how the symbology rules worked. Here are the parcels of Heartland Circle, made to look like a pencil sketch.

![](/media/30daychallenge-2024/day2-2.jpg)

After my initial post of this on [Mapstodon](https://mapstodon.space), someone pointed out that the labels were a bit *too* consistent for the style, so I brought them into Inkscape and applied some randomized simplification on them. It's still not perfect, but the letters are at least a bit different from one another.

## Day 3: Polygons

I turned the County's address points (pulled from OpenStreetMap!) into Voronoi polygons, then symbolized them by the `addr:housenumber` tag.

![](/media/30daychallenge-2024/day3.jpg)

The result was actually a bit interesting. See, the County is in charge of addresses in unincorporated areas, and assigns them based on a County-wide grid, ascending in number as you proceed south and west. The result was a short of dovetailed gradient. Plus these low-number "holes" where the cities are. Except for Plainfield, their numbers were unusually high…

Out of curiousity, I made the same map, but flipping the color ramp and scaling it to focus more on the low-numbers. Here's that:

![](/media/30daychallenge-2024/day3-1.jpg)

## Day 6: Raster

Here's the Illinois State Geological Survey's landcover raster on the left. Nothing fancy.

On the right, the results of running Q's *Zonal Histogram* tool with 2.5km² zones, then re-displaying the pixels by count.

![](/media/30daychallenge-2024/day6.jpg)

## Day 7: Vintage

Here are animated vintages of TIGER data.

![](/media/30daychallenge-2024/day7.gif)

## Day 8: HDX

The Humanitarian Data Exchance. *Lots* of really amazing data in here.

I chose to map out the World Risk Index’s Vulnerability Score, then used Q's geometry generator to add little arrows, showing how much that score has changed since 2000.

![](/media/30daychallenge-2024/day8.jpg)

## Day 9: AI

I fed the AI a legal description as a prompt and asked it to give me a map depiction of the property described. The results are about what I expected. I've still got job security.

![](/media/30daychallenge-2024/day9-1.jpg)
![](/media/30daychallenge-2024/day9-2.jpg)
![](/media/30daychallenge-2024/day9-3.jpg)

## Day 10: Pen and Paper

I decided to draw the same legal from the AI prompt, by hand. I had to start over, and still ended up making a mistake, so some tape and paper came in handy.

![](/media/30daychallenge-2024/day10.jpg)

Other tools were used as well. Our office is full of the old-school cartography tools that were formerly used in creating our tax maps. That little triangle ruler has "feet" measurements on the edges, in common tax map scales, and then it's full of conversions between units, etc. Really handy when you don't have a computer to do it all for you!

![](/media/30daychallenge-2024/day10-1.jpg)

## Interlude

I was busy drawing subdivisions this month.

## Day 20: OpenStreetMap

This map actually gets its own write-up / talk elsewhere, but I've got to tie in the ends on the post before it goes up. But it's 100% OpenStreetMap, or a dynamic PostGIS expression based on an OSM feature.

![](/media/30daychallenge-2024/day20.jpg)

## Day 22: Two Colors

Here's the State of Illinois, drawn entirely with OpenStreetMap `natural=water` and `natural=wood` features.

![](/media/30daychallenge-2024/day22.jpg)

## Day 23: From Memory

A map drawn entirely from memory. I mapped out the way we biked to the local ice cream place, something that will forever be seared into my memory.

![](/media/30daychallenge-2024/day23.jpg)

This was my first time using QGIS's annotations. By creating "text on line" annotations and using the "freehand line" drawing tool, I was able to make labels that, while digital, still had a floaty, crappy hand-done look about them. I like it!

## Day 26: Map Projections

In my undergrad years, I saw this *thing* in my school's old "map library", which was a room largely unused, poorly organized, and bursting with stuff nobody could get rid of. The thing was a clear plastic globe with countries drawn on it in black, with a light bulb in the center.

It was used to demonstrate map projections by *literally projecting them*. By holding up a cylinder, cone, or other angled plane, you could show how various map types related back to the globe itself, and interactively adjust your projection by moving the plane around or bending it.

I wanted to play with that idea in Blender. If I had more time, I thought about trying other things, like animations, or even baking the shadows onto curves and then flattening them out. Another time, maybe.

![](/media/30daychallenge-2024/day26.jpg)

## Day 27: Micromapping

Another map that existed in the past, but I recreated it fresh for this year. Here is the Kendall County Government Campus, as micromapped in OpenStreetMap.

Hard to see without being able to zoom in, but the trees are also using some fun symbology. With a randomized wave on buffered points, we get these fun dynamic tree shapes without too much fuss!

![](/media/30daychallenge-2024/day27.jpg)