---
title: Media Log
description: Reading things. Fleshing out details as we go.
date: 2024-04-13T21:24:25-05:00
draft: false
toc: true
weight: 90
---

<!-- To Read 
{{< book title="" author="" >}}
-->

<!-- Reading 
{{< book title="" author="" >}}
{{< book title="Hip Hop Family Tree" author="Ed Piskor" >}}
{{< book title="The Overstory" author="Richard Powers" >}}
{{< book title="Practicing the Way" author="John Mark Comer" >}}
{{< book title="20,000 Leagues Under The Sea" author="Jules Verne" >}}
-->

## 2025
{{< booklist >}}


{{< book title="Mistborn" author="Brian Sanderson" date="Feb 2025" rating="3.5" >}}
{{< book title="Riots I Have Known" author="Ryan Chapman" date="Feb 2025" rating="4" >}}
{{< book title="City of Thieves" author="David Benioff" date="Feb 2025" rating="4.5" >}}
{{< book title="The Hopkins Manuscript" author="R.C. Sherriff" date="Jan 2025" rating="4" >}}
{{< book title="Travels with Charley" author="John Steinbeck" date="Jan 2025" rating="3.5" >}}
{{< book title="In the Distance" author="Hernan Diaz" date="Jan 2025" rating="4.5" >}}
{{< book title="Vilest Things" author="Chloe Gong" date="Jan 2025" rating="3.5" >}}
{{< book title="Immortal Longings" author="Chloe Gong" date="Jan 2025" rating="4" review="cyberpunk + palace intrigue + hunger games, sorta? really fun" >}}

{{< /booklist >}}

## 2024
{{< booklist >}}

{{< book title="A Stroke of the Pen" author="Terry Pratchett" date="Dec 2024" rating="4" >}}
{{< book title="River of Silver" author="S. A. Chakraborty" date="Dec 2024" rating="4" >}}
{{< book title="Lock In" author="Jon Scalzi" date="Nov 2024" rating="4.5" >}}
{{< book title="After World" author="Debbie Urbanski" date="Oct 2024" rating="5" review="Truly unlike anything I have read before. It was in turns delightful and unnerving.">}}
{{< book title="On the Edge of the Dark Sea of Darkness" author="Andrew Peterson" date="Oct 2024" rating="3.5" review="A fantasy story that doesn't take itself too seriously, while still being earnest. Reminiscent of Terry Pratchett in a way, but minus all the United Kingdom vibes.">}}
{{< book title="Empire of Gold" author="S. A. Chakraborty" date="Oct 2024" rating="4" review="There was a lot of 'teleporting' that I feel like shows up late in these kinds of stories, which irks me. That aside, this was a gratifying conclusion to an incredible trilogy!">}}
{{< book title="The Kaiju Preservation Society" author="John Scalzi" date="Oct 2024" rating="4.5" review="First time reading Scalzi, and it was a treat! Loved the dialogue style and all the banter between characters.">}}
{{< book title="Kingdom of Copper" author="S. A. Chakraborty" date="Oct 2024" rating="4">}}
{{< book title="City of Brass" author="S. A. Chakraborty" date="Sep 2024" rating="4" >}}
{{< book title="The Wilderness" author="Samantha Harvey" date="Sep 2024" rating="3.5" >}}
{{< book title="The Western Wind" author="Samantha Harvey" date="Aug 2024" rating="4" >}}
{{< book title="Andre the Giant" author="Box Brown" date="Aug 2024" rating="3.5" >}}
{{< book title="Is This Guy For Real?" author="Box Brown" date="Aug 2024" rating="4" >}}
{{< book title="Dear Thief" author="Samantha Harvey" date="Aug 2024" rating="4" >}}
{{< book title="Paying The Land" author="Joe Sacco" date="Aug 2024" rating="4.5" >}}
{{< book title="Tetris" author="Box Brown" date="Aug 2024" rating="4" >}}
{{< book title="Pulp" author="Ed Brubaker & Sean Phillips" date="Aug 2024" rating="4" >}}
{{< book title="The World We Make" author="N. K. Jemisen" date="Jul 2024" rating="3.5" >}}
{{< book title="The Adventures of Amina Al-Sirafi" author="Shannon Chakraborty" date="Jul 2024" rating="4.5" review="A really fun read, well worth the hours of sleep I gave up for it. I loved the characters, the narrative framing, and the fact that we're set up for a potential series.">}}
{{< book title="Makers" author="Cory Doctorow" date="Jul 2024" rating="3" >}}
{{< book title="All The Light We Cannot See" author="Anthony Doerr" date="Jul 2024" rating="4.5" >}}
{{< book title="Orbital" author="Samantha Harvey" date="Jun 2024" rating="4.5" review="It was so beautifully written, I felt I had to start keeping track of books again, to remember I'd read this." >}}
{{< book title="The City We Became" author="N. K. Jemisen" date="May 2024" rating="4" >}}
{{< book title="The Hundred Thousand Kingdoms" author="N. K. Jemisen" date="May 2024" rating="4" >}}
{{< book title="The Killing Moon" author="N. K. Jemisen" date="May 2024" rating="3.5" >}}
{{< book title="Every Heart A Doorway" author="Seanan McGuire" date="Apr 2024" rating="4" >}}
{{< book title="System Collapse" author="Martha Wells" date="Apr 2024" >}}
{{< book title="Before Mars" author="Emma Newman" date="Mar 2024" rating="4" >}}
{{< book title="After Atlas" author="Emma Newman" date="Feb 2024" rating="4" >}}
{{< book title="Planetfall" author="Emma Newman" date="Feb 2024" rating="4.5" >}}
{{< book title="Middlegame" author="Seanan McGuire" date="Feb 2024" rating="4" >}}
{{< book title="The Long War" author="Terry Pratchett & Stephen Baxter" date="Jan 2024" >}}
{{< book title="The Long Earth" author="Terry Pratchett & Stephen Baxter" date="Jan 2024" >}}
{{< book title="The Stone Sky" author="N. K. Jemisen" date="Jan 2024" >}}

{{< /booklist >}}

## 2023
{{< booklist >}}

{{< book title="The Obelisk Gate" author="N. K. Jemisen" date="Dec 2023" >}}
{{< book title="The Ballad Of Perilous Graves" author="Alex Jennings" date="Nov 2023" >}}
{{< book title="Old Babes In The Woods" author="Margaret Atwood" date="Nov 2023" >}}
{{< book title="Witch King" author="Martha Wells" date="Oct 2023" >}}
{{< book title="Moon Witch, Spider King" author="Marlon James" date="Aug 2023" >}}
{{< book title="The Fifth Season" author="N. K. Jemisen" date="Aug 2023" >}}
{{< book title="Fairy Tale" author="Stephen King" date="Jul 2023" >}}
{{< book title="Zero Days" author="Ruth Ware" date="Jul 2023" >}}
{{< book title="Lapvona" author="Otessa Moshfegh" date="Jul 2023" >}}
{{< book title="Project Hail Mary" author="Andy Weir" date="Jun 2023" >}}
{{< book title="Open Borders" author="Bryan Caplan" date="May 2023" >}}
{{< book title="Karen Memory" author="Elizabeth Bear" date="May 2023" >}}
{{< book title="Soonish" author="Kelly Weinersmith" date="May 2023" rating="4" >}}
{{< book title="Bea Wolf" author="Zach Weinersmith" date="Apr 2023" rating="5" >}}
{{< book title="Black Leopard, Red Wolf" author="Marlon James" date="Feb 2023" rating="5" >}}
{{< book title="Cloud Cuckoo Land" author="Anthony Doerr" date="Jan 2023" rating="5" >}}
{{< book title="Artemis" author="Andy Weir" date="Jan 2023" rating="4.5" >}}
{{< book title="A Prayer For The Crown-Shy" author="Becky Chambers" date="Jan 2023" rating="4" >}}
{{< book title="The Origin Of Storms" author="Elizabeth Bear" date="Jan 2023" rating="3.5" >}}

{{< /booklist >}}

## 2022
{{< booklist >}}

{{< book title="The Red-Stained Wings" author="Elizabeth Bear" date="Dec 2022" rating="3.5" >}}
{{< book title="The Stone In The Skull" author="Elizabeth Bear" date="Dec 2022" rating="4" >}}
{{< book title="All The Wind-Wracked Stars" author="Elizabeth Bear" date="Dec 2022" rating="4" >}}
{{< book title="Machine" author="Elizabeth Bear" date="Nov 2022" rating="4.5" >}}
{{< book title="Steles Of The Sky" author="Elizabeth Bear" date="Nov 2022" rating="4" >}}
{{< book title="Shattered Pillars" author="Elizabeth Bear" date="Nov 2022" rating="4" >}}
{{< book title="Ancestral Night" author="Elizabeth Bear" date="Nov 2022" rating="4.5" >}}
{{< book title="Range Of Ghosts" author="Elizabeth Bear" date="Oct 2022" rating="4.5" >}}
{{< book title="The Sculptor" author="Scott McCloud" date="Sep 2022" rating="3.5" >}}
{{< book title="Roughneck" author="Jeff Lemire" date="Sep 2022" rating="4" >}}
{{< book title="The Fixer, And Other Stories" date="Joe Sacco" date="Sep 2022" rating="5" >}}
{{< book title="The New Deal" author="Jonathan Case" date="Sep 2022" rating="3" >}}
{{< book title="What Is The What" author="Dave Eggers" date="Aug 2022" rating="4.5" >}}
{{< book title="A Tale For The Time Being" author="Ruth Ozeki" date="Aug 2022" rating="4.5" >}}
{{< book title="The Book Of Form And Emptiness" author="Ruth Ozeki" date="Jul 2022" rating="4.5" >}}
{{< book title="Earthlings" author="Sayaka Murata" date="Jul 2022" rating="4" >}}
{{< book title="To Be Taught, If Fortunate" author="Becky Chambers" date="Jul 2022" rating="4" >}}
{{< book title="The Galaxy, And The Ground Within" author="Becky Chambers" date="Jul 2022" rating="4.5" >}}
{{< book title="Record Of A Spaceborn Few" author="Becky Chambers" date="Jul 2022" rating="3.5" >}}
{{< book title="A Closed And Common Orbit" author="Becky Chambers" date="Jun 2022" rating="4.5" >}}
{{< book title="The Long Way To A Small, Angry Planet" author="Becky Chambers" date="Jun 2022" rating="4" >}}
{{< book title="A Psalm For The Wild-Built" author="Becky Chambers" date="Jun 2022" rating="5" >}}
{{< book title="Your Fathers, Where Are They? And the Prophets, Do They Live Forever?" author="Dave Eggers" date="3.5" date="Jun 2022" >}}
{{< book title="The Parade" author="Dave Eggers" date="Jun 2022" rating="4" >}}
{{< book title="The Every" author="Dave Eggers" date="May 2022" rating="4" >}}
{{< book title="Fugitive Telemetry" author="Martha Wells" date="May 2022" rating="4" >}}
{{< book title="Network Effect" author="Martha Wells" date="May 2022" rating="5" >}}
{{< book title="Lost Maramech And Earliest Chicago" author="John Steward" date="May 2022" rating="3.5" >}}
{{< book title="Exit Strategy" author="Martha Wells" date="Apr 2022" rating="4" >}}
{{< book title="Rogue Protocol" author="Martha Wells" date="Apr 2022" rating="4" >}}
{{< book title="The Circle" author="Dave Eggers" date="Apr 2022" rating="4.5" >}}
{{< book title="Ball Lightning" author="Cixin Liu" date="Apr 2022" rating="4" >}}
{{< book title="A Man Called Ove" author="Fredrik Backman" date="Apr 2022" rating="4" >}}
{{< book title="Artificial Condition" author="Martha Wells" date="Apr 2022" >}}
{{< book title="The Wandering Earth" author="Cixin Liu" date="Mar 2022" >}}
{{< book title="All Systems Red" author="Martha Wells" date="Mar 2022" >}}
{{< book title="Anxious People" author="Fredrik Backman" date="Mar 2022" >}}
{{< book title="The Winter Room" author="Gary Paulsen" date="Feb 2022" rating="5" >}}

{{< /booklist >}}

## 2021
{{< booklist >}}

{{< book title="Revenant Gun" author="Yoon Ha Lee" date="Nov 2021" >}}
{{< book title="Raven Stratagem" author="Yoon Ha Lee" date="Nov 2021" >}}
{{< book title="Monstrous Regiment" author="Terry Pratchett" date="Oct 2021" >}}
{{< book title="Ninefox Gambit" author="Yoon Ha Lee" date="Oct 2021" >}}
{{< book title="A Most Remarkable Creature" author="Jonathan Meiburg" date="Oct 2021" rating="5" >}}
{{< book title="Waste Tide" author="Quifan Chen" date="Sep 2021" >}}
{{< book title="Death's End" author="Cixin Liu" date="Sep 2021" >}}
{{< book title="The Hidden Girl and Other Stories" author="Ken Liu" date="Sep 2021" >}}
{{< book title="The Dark Forest" author="Cixin Liu" date="Aug 2021" >}}
{{< book title="Vagabonds" author="Jingfang Hao" date="Aug 2021" >}}
{{< book title="The Three-Body Problem" author="Cixin Liu" date="Jul 2021" >}}
{{< book title="The Grace of Kings" author="Ken Liu" date="Jul 2021" >}}
{{< book title="The Paper Menagerie and Other Stories" author="Ken Liu" date="Jun 2021" >}}
{{< book title="Homesick For Another World" author="Otessa Moshfegh" date="Jun 2021" >}}
{{< book title="Eileen" author="Otessa Moshfegh" date="May 2021" >}}
{{< book title="My Year Of Rest And Relaxation" author="Otessa Moshfegh" date="May 2021" >}}
{{< book title="Ready Player Two" author="Ernest Cline" date="May 2021" >}}
{{< book title="Death In Her Hands" author="Otessa Moshfegh" date="Apr 2021" rating="4.5" >}}
{{< book title="Ready Player One" author="Ernest Cline" date="Apr 2021" >}}
{{< book title="Pilgrim At Tinker Creek" author="Annie Dillard" date="Mar 2021" >}}

{{< /booklist >}}

## Older

Prior to 2021, I wasn't really keeping track. But if I remember a book from years past, I'll add it!

{{< booklist >}}

{{< book title="A World Lost" author="Wendell Berry" date="Sep 2020" >}}
{{< book title="Jayber Crow" author="Wendell Berry" date="Aug 2020" >}}
{{< book title="Wayfinding" author="M. R. O'Connor" date="Mar 2020" >}}
{{< book title="The Old Ways: A Journey On Foot" author="Robert Macfarlane" date="Dec 2019" >}}
{{< book title="1Q84" author="Haruki Murakami" date="Oct 2019" >}}
{{< book title="H is for Hawk" author="Helen Macdonald" date="2016" rating="4.5" >}}
{{< book title="Mind Of The Raven" author="Bernd Heinrich" date="Oct 2012" rating="5" >}}
{{< book title="Watership Down" author="Richard Adams" rating="5" >}}

{{< /booklist >}}