---
title: ''
date: 2024-04-13T21:24:25-05:00
draft: false
---

Josh is a GIS Developer in the public sector, where he gets to nudge boundary lines around and build things that (hopefully) make Kendall County run a little better.

When he's not on the clock, you can find him mapping on OpenStreetMap, fiddling with things, knitting, or talking too much to his wife and sons about any of the above.

## Interests

- GIS
- OpenStreetMap
- Handicrafts
- FOSS
- Dumpster Diving
- Botany
- Foraging
- Birds