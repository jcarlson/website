---
title: 'Talks'
description: Presentations, workshops, &c
date: 2024-10-23T21:09:00-05:00
toc: false
weight: 50
---

Here's a list of presentations and workshops I've given in the past. I like to use RevealJS and Quarto for these, which makes them a lot more JS-heavy than the rest of this site, so they live over in their own subdomain, https://slides.jcarlson.page.

Tip: If you're viewing a slide deck, hit the **S** key to view my speaker notes. I'd be a bad speaker if *everything* I said was in the slides themselves; the notes will give you a bit more context and information.

---

{{< talklist >}}

{{< talk
    title="Intro To QGIS"
    url="https://slides.jcarlson.page/intro-qgis/index.html"
    description="A beginner’s guide to QGIS"
    presented="ILGISA Annual Conference 2024"
>}}

{{< talk
	title="Shared Instances & ArcGIS Server"
    url="https://slides.jcarlson.page/shared-instances/index.html"
    description="An overview of what shared instances are, when to use them, and how you stand to benefit"
    presented="ILGISA Annual Conference 2024"
>}}

{{< talk
	title="Let’s Read a Legal!"
    url="https://slides.jcarlson.page/legal-descriptions/index.html"
    description="A quick overview of common legal description characteristics and how to interpret them"
    presented="ILGISA Annual Conference 2024"
>}}

{{< talk
	title="Intro To OpenStreetMap"
    url="https://slides.jcarlson.page/intro-osm/index.html"
    description="A whirlwind tour of what OpenStreetMap is, how it works, and how to contribute to the project."
    presented="ILGISA Annual Conference 2024"
>}}

{{< talk
	title="Your Very Own OSM!"
    url="https://slides.jcarlson.page/osm-extract/workshop.html"
    description="A step-by-step guide to using osm2pgsql and osmium to create a regional extract of OpenStreetMap"
    presented="FOSS4GNA 2024, ILGISA Annual Conference 2023"
>}}

{{< talk
	title="Leveraging OpenStreetMap for Asset Inventories"
    url="https://slides.jcarlson.page/osm-assets/index.html"
    description="Imagining what asset management could look like with OpenStreetMap"
    presented="ILGISA Regional Meetup 2024"
>}}

{{< talk
	title="Kendall County on OSM"
    url="https://slides.jcarlson.page/osm-assets/index.html"
    description="A brief overview of the various ways that Kendall County uses and contributes to OpenStreetMap"
    presented="OSMUS Government Working Group April 2024 Meeting"
>}}

{{< talk
	title="Have it Both Ways"
    url="https://slides.jcarlson.page/qgis-ags-connection/index.html"
    description="Demonstrating how to add ArcGIS Feature Service connections to QGIS for viewing and editing, and discussing some of the more interesting QGIS features that make it worth doing."
    presented="ILGISA Webinar February 2024"
>}}

{{< talk
	title="Open Sourcing Farm Card Calculations"
    url="https://slides.jcarlson.page/farm-cards/index.html"
    description="In which we take a legacy proprietary workflow and recreate it with GeoPandas"
    presented="FOSS4GNA 2023, ILGISA Annual Conference 2022"
>}}

{{< talk
	title="Developing Digital Restaurant Inspections"
    url="https://slides.jcarlson.page/restaurant-inspections/index.html"
    description="Describing the development process of Kendall County’s digital restaurant inspection process, which was successfully deployed in 2023."
    presented="ILGISA Annual Conference 2023"
>}}

{{< talk
	title="Creating a Local OSM Extract"
    url="https://slides.jcarlson.page/osm-extract/index.html"
    description="An overview of OpenStreetMap and how to extract and manipulate the data for an area of interest."
    presented="ILGISA Regional Meetup 2023"
>}}

{{< talk
	title="Herding Cats 2023"
    url="https://slides.jcarlson.page/herding-cats-2023/index.html"
    description="How we used the OSM Tasking manager to coordinate some mapping efforts with our interns."
    presented="ILGISA Annual Conference 2023, FOSS4GNA 2023"
>}}

{{< talk
	title="Serverless XYZ Basemaps"
    url="https://slides.jcarlson.page/serverless-tiles/index.html"
    description="A quick how-to on generating XYZ basemap tiles and serving them through Amazon Web Services S3"
    presented="ILGISA Annual Conference 2023"
>}}

{{< talk
	title="Dashboards and Data Expressions"
    url="https://slides.jcarlson.page/data-expressions/index.html"
    description="Showcasing the various ways of using Data Expressions in ArcGIS Dashboards, both useful and silly"
    presented="ILGISA Annual Conference 2022"
>}}

{{< talk
	title="Hosted Copies, Targeted Updates"
    url="https://slides.jcarlson.page/hosted-copy/index.html"
    description="Updating ArcGIS Online / Portal layers from disparate other sources"
    presented="ILGISA Annual Conference 2022"
>}}

{{< /talklist >}}