---
title: "Recap of the 30 Day Map Challenge - 2023"
description: "Some day I'll get all 30..."
author: "Josh Carlson"
date: "2023-12-12"
tags:
    - maps
    - cartography
    - 30daymapchallenge
    - reflection
aliases:
    - /blog/posts/30day2023-recap
---

Well, another 30 Day Map Challenge has come and gone. How did I do?

You can take a look at the [portfolio page for the challenge](/portfolio/30daymapchallenge-2023/) to see all the maps.

I regret to say, I didn't even hit 50% this year. It doesn't help that November is Off The Rails Month at Kendall County, with everyone trying to wrap things up for the end of the fiscal year. Maybe next year I'll just work ahead and schedule my posts.

That said, for the 12 maps I *did* manage to make, I had a lot of fun! I got to really stretch the ways I use QGIS's *geometry generator* tool for symbology. For example, I really like the "woods" style I got in this disc golf map:

![](/media/30daychallenge-2023/day15.jpg)

My *favorite*, map, though? Definitely the animated magnetic declination isolines from Day 22.

{{<video src="/media/30daychallenge-2023/day22.mp4" type="video/mp4">}}

# Looking Elsewhere

The best part of the 30 Day Map Challenge, for me, isn't making maps, it's seeing everyone else's! I've said it before, but I'm no cartographer, and I'm not even a data scientist. If I want to see really amazing maps, I've gotta look elsewhere. Following the challenge hashtag on Mastodon has been even better this year, thanks to an expanding user base over there.

Things I saw in others' posts that I may want to try:

- Picking a single topic / data source to use each day
- Use a particular method each day (R, Blender, Python)
- Don't worry about making it "interesting", just make it look *nice*. Some of the best maps I saw had nothing to say other than "check out this nice map", and that was totally fine.
- Try to be a bit more minimal
- If a certain day's topic is just really interesting or fun, make *more than one map*!

And seriously, I gotta make these maps ahead of time. November is nuts over here.