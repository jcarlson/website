---
title: 'Vikkel Mittens'
date: 2024-05-24T19:41:56-05:00
toc: true
tags:
    - crafts
    - knitting
    - mittens
---

It's May, and we're having temperatures in the 80s. I guess I'm just a *little* late posting about my new mittens. They're great, though!

![A pair of mustard yellow mittens on a wooden table.](/media/crafts/vikkel-mittens1.jpg)

## Process

I bike year-round, and I was *years* overdue for replacing my biking gloves. I can be pretty tough on my things, so I wanted something durable. Some years ago, I bid, sight-unseen, on some estate auction online, and became the owner of many cones of some seriously *gnarly* rug yarn. "That should be durable enough!"

It definitely was, but I failed to consider how unpleasant an experience hand-knitting with rug yarn would be. Like grinding my fingers into a dish scrubber. I decided to make the mittens extra big, and leave room for a liner.

## The Curve

I love short-rows in knitting. I love to really tailor a project and get it to fit *just right*. I don't use patterns, so I just kind of go with how I feel.

For these mittens, I knew I'd be using them mostly for biking. The *default state* would be grasping a handle. From the thumb to the fingertips, I added periodic short-rows, to give the mittens a natural curved shape.

![A single mitten, seen from the side. The mitten has an obvious curve to it, as if it were grasping something.](/media/crafts/vikkel-mittens2.jpg)

I wanted to do *something* to accentuate the curvature of the mittens, and I remembered something called the **vikkel braid**. It's like doing a bind-off while also continuing to work? I put one along each short-row, and the effect was rather nice. [^1]

[^1]: Has me thinking about shells or horns or something.

![A single mitten, seen from the side. The back of the hand has a series of raised ridges.](/media/crafts/vikkel-mittens3.jpg)

## The Liner

At first, I thought I'd just knit a second pair of mittens to keep the rug yarn from completely grating off my skin. But I'm too lazy for that!

I had a pair of much-loved socks. I'd darned, and darned, and darned the things, but they were finally felting too far, getting too tight in the insteps to put on. They were *perfect*. I cut the heels out, sewed up the gap, and cut some holes for my thumbs. Liners! [^2]

[^2]: Yeah, my thumbs are still a little cold. But that's next winter's problem!

![An old sock, with many darns on it. It's had its heel cut out and a hole cut into the side for a thumb.](/media/crafts/sock-of-theseus.jpg)

They're starting to get that wooly halo to them, already a bit worn from when they were finished. But they're holding up well!