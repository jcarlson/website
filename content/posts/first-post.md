---
title: "First Post"
author: "Josh Carlson"
date: "2023-08-04"
tags: [news]
aliases:
    - /blog/posts/first-post
---

> "Meaningless! Meaningless!"
>   says the Teacher.
> "Utterly meaningless!
>   Everything is meaningless."
>
>   -- Ecclesiastes 1:2

I pretty much stand by that, and I recognize that expressing oneself online is about as meaningless as it gets. But I thought maybe I'd start a blog anyway! It wasn't originally what I planned for my site here, but I recently switched over to using [Quarto](https://quarto.org/) for static site generation, and the templates seemed nice enough.

I was thinking the other day about some of the posts I make elsewhere. Mostly, I am unconcerned about the fate of that content. But sometimes I post a thing and think, "this is pretty neat". I like those things, small as they are, and it bothers me that it's essentially free content I provide for those other places. Especially when the ToS of a place explicitly gives away any rights I might have to dictate how or where the content is used.

Not all of those little things are worth a *portfolio* item, but maybe a short post here or there. Anyway, that's all this is really about. In a time when most of the centralized web seems to be getting weirder and worse, I just want a little space I can claim for myself.