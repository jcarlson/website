---
title: "Revisiting the Horlick Dam"
author: "Josh Carlson"
date: "2023-10-14"
tags: [update, reflection]
aliases:
    - /blog/posts/revisiting-horlick
---

Out of the blue, I decided to put [an older project of mine](/portfolio/horlick-dam) into my Portfolio here.

![](/media/horlick/tinmap.jpg)

# Background

In my final year at UW Parkside (2016-2017 academic year), I got a little *ambitious*. I was already pretty invested in the Root River: I was doing fieldwork there, I worked summers at a boat rental place on the Root, and it ran through Racine, the city I lived in.

I knew that the Wisconsin DNR was talking about taking out the **Horlick Dam** owing to some structural issues. Locals accustomed to a large, placid impoundment were not happy about this prospect. Sportsmen concerned with the health of the aquatic ecosystem wanted to see it happen.

## A Twist

Another bit of news came up that might change the situation: the Great Lakes Compact had [agreed to a diversion of water to Waukesha, Wisconsin](https://greatlakes.org/2016/06/states-approve-request-to-divert-great-lakes-water/). As part of this agreement, Waukesha would return its treated wastewater to the Root River, as opposed to the Fox. This would increase the baseline flow of the Root.

## The Question

The topic of the Horlick Dam was being discussed, but it seemed to me that this was a crucial missing piece of the dialogue. What would the river look like *without* the dam, but *with an increased flow*?

Anyway, you can go read the Portfolio item for the details. It was probably more than an undergrad should have attempted, but I'm still pleased with it overall.

# Lesson Learned (The Hard Way)

**Save your work. Remember where you saved it.**

I was able to find a few emails with the poster attached to it, but my GIS files? My data? The cool 3D animation flyby, showing fluctuating water levels?

*All gone.* Or, as near as I can tell. There may be a thumb drive kicking around some junk drawer, waiting to be discovered.

It's not as though I want to re-run the analysis, but at the same time, all I have to show for my work is the poster's images and charts now. It would have been nice to save the raw data.

# Looking Back from Here

As of writing this (14 Oct 2023), it appears that the Army Corps of Engineers is still pushing for **Full Removal** of the dam. A [report from June 2023](https://www.lrc.usace.army.mil/Portals/36/docs/projects/Horlick%20Dam/PublicMeeting_Horlick_14June2023.pdf) reveals considerably more planning and research than was available 6 years ago. According to the project website, it may move to the "Design" phase next year.

Notably, the sediment has been tested and deemed safe! So they can let the sediment gradually wash down without significant impacts to the lower river and lake systems. I'll be curious to see if the future maps look anything like my model.

And Waukesha? It's kind of strange that this project came to mind just now, because they turned that pipe "on" [just five days ago](https://greatlakes.org/2023/10/waukeshas-diversion-of-lake-michigan-water-highlights-the-importance-of-the-great-lakes-compact/). Something to keep an eye on…