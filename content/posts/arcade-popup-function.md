---
title: Arcade Popup Field Lists — Cool, but also Annoying
description: In which I try to make it slightly less annoying
author: Josh Carlson
date: 25 Mar 2024
updated: 15 Apr 2024
tags:
    - arcgis
    - arcade
    - functions
aliases:
    - /blog/posts/arcade-popup-function
---

A bit ago, AGOL / ArcGIS Enterprise introduced a popup element with *power*. The whole thing could be defined by a good Arcade expression, but still have the familiar output of a field list or chart. Expression based-field lists and charts can be *dynamic*! Null fields? Have your expression just drop it from the list!

This was a good move, and something we're using a lot. But there's something annoying about it. Check out the field-list template:

```js
return {
    type: 'fields',
    //title: '',
    //description : '',
    fieldInfos:  [{
            fieldName: "att1"  // fieldName should match the key in the attributes dictionary
        },
        {
            fieldName: "att2"  // fieldName should match the key in the attributes dictionary
        },
        {
            fieldName: "att3"  // fieldName should match the key in the attributes dictionary
        }],
    attributes : {att1 : 2, att2 : 3, att3 : 4}  // replace this dictionary with your own key-value pairs
}
```

We've got an array of dicts, `fieldInfos`, each of which consists of a single string in the `fieldName` key. And we a dict, `attributes`. The fieldName text should match the keys in the attributes dict. There is nothing else in the fieldInfos dicts.

So… what is `fieldInfos` actually doing? The information it stores is identical to the list of keys from `attributes`. Why we can't just submit `attributes` itself and infer the field names from the keys, I don't know. I hate it.

When I add something to the attributes dict, I want it added to the fieldInfos array, too. Or better yet, I don't want to be concerned with a redundant object at all. So I wrote a couple of helper functions.


## Load Value

I started with the empty objects just combined both steps into one. Simple enough:

```js
var fieldInfos = []
var attributes = {}

function loadValue(fieldname, value) {
	Push(fieldInfos, {fieldName: fieldname})
	attributes[fieldname] = value
}

// do your stuff here
loadValue('Field 1', 'some text')
loadValue('Field 2', 123.456)

return {
  type: 'fields',
  fieldInfos,
  attributes
}
```

But even that was jankier than I wanted. How about we just take the dict keys and make the array from it?


## Dict to Field-List

```js
function buildFieldList(dict) {
  var out_arr = []
  for (var key in dict) {
    Push(out_arr, {fieldName: key})
  }

  return out_arr
}

var attributes = {
    'Field 1': 'some text',
    'Field 2': 123.456
}

return {
    type: 'fields',
    title: 'My Excellent Field List'
    fieldInfos: buildFieldList(attributes),
    attributes
}
```

Really makes you wish this was straight JavaScript, where you could just call `Object.keys()` or something…

## UPDATE

I was implementing this the other day, and realized why the secondary array was useful. When you loop through a dictionary, it takes the keys *alphabetically*. The `fieldInfos` object is the only thing that retains the attribute display order.

It's still annoying, but it's tolerable. `loadValue()` it is, then.