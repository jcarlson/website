---
title: "Garbage In, Organized Garbage Out"
description: "Getting County data on the map"
date: "2024-03-23"
toc: true
tags:
    - kendall county
    - python
    - sql
aliases:
    - /blog/posts/devnet-extract
---

## Moving to a Shared Instance Model

In the early days of my job at Kendall County, we made a big shift in how we ran our department. The maps and layers all used to come through ArcGIS Server 10.2.2, and every layer in the map was a dedicated Map Service. Our parcels layer, being used every day? Dedicated service. A "dead birds survey" layer from a few years ago that almost nobody was using? Also a dedicated service. All this running on an *ancient* SQL Server that crashed several times a day. Uptime was a joke.

```goat
     .--------.
     | GIS DB +--.
     '--------'  |    .-----------.    .----------.
                 +-->| Map Service +-->| Web Maps |
.-------------.  |    '-----------'    '----------'
| Assessments +--'
'-------------'
```

We moved all our stuff to ArcGIS Enterprise, which comes with what Esri calls the *ArcGIS DataStore*. This is really just a wrapper around PostgreSQL + PostGIS with some custom Esri extensions, but the key thing about it is that layers published there are part of the server's *shared instance pool*. The server manages access to these layers collectively, rather than keeping dedicated processes running for each.

There are other reasons performance improved, but this was a big part of it. Our server went from maxing out every few hours to rarely crossing 20% CPU.


## Broken Link

There was one benefit to the old model: live data. As parcels were edited, the changes reflected on the map immediately. The parcel services were published with ad-hoc attribute joins to the County's assessments database, too, so an update to ownership, assessment level, tax code, *everything* updated in near real time.

*This was not always desirable, like when there were mistakes that could have been caught and fixed before hitting the public maps, but in general, it was convenient.*

With the DataStore? Not anymore. The layers there needed to be refreshed or overwritten to bring in new data from external sources like the assessments database.

```goat
     .--------.
     | GIS DB +--.     ____
     '--------'  |    /    \     .---------.    .----------.
                 +-->+ ETL? +-->| DataStore +-->| Web Maps |
.-------------.  |    \____/     '---------'    '----------'
| Assessments +--'
'-------------'
```

I've since learned that this is a very common scenario in GIS departments like mine. The ETL process that we used in the beginning was to have an ArcGIS Pro project with the desired data sources loaded in, joins, applied, etc., and to use that project to completely overwrite the layers in the DataStore.

### Why This Stinks

1. It breaks or fails a lot, sometimes in a way that corrupts the DataStore layer
2. Even if the overwrite is successful, it's still pretty easy to republish in a way that breaks downstream maps, apps., etc
3. It's pretty much a manual process. You can try to schedule it with ArcGIS Pro, but I *loathe* using ArcPy, and have never had luck with it.
4. We were overwriting the *entire* layer, when on a given day, maybe only a few hundred parcels were actually being changed


## The ArcGIS Python API

### A Good Start

I wasn't hired as a developer, but I had some decent experience with the ArcGIS Python API for working with layers in a DataStore. I figured there *had* to be a way to work this process with that instead, and outside of the ArcGIS Pro environment.

It was quite the saga figuring out how to do it, but it's not terribly interesting. Suffice to say, I was still learning a lot about Python, and it really showed. But I got it working! The layers in our public maps could update with the County's source data, and in a way that I didn't have to attend to. We moved our update schedule to occur *nightly*, rather than me setting aside a couple hours every Friday to do it.

I gave [a talk about it](https://github.com/jdcarls2/ilgisa-2022/tree/main/hosted-copy) back in 2022. That was before this site was started, so I haven't copied the talk over as of writing this, but I plan to at some point. Here's the idea:

1. Query both layers to dataframes
2. Use `pandas.DataFrame.compare` to identify changed rows
3. Adds, updates, and deletes via the ArcGIS Python API

So now we're only editing the rows with changes, not overwriting anything, and it only takes a couple minutes. Improvement!

### A Bad End

"End" makes it sound sudden, but this thing slowly and painfully ground to a halt. Why?

Well, it seems like the version of the ArcGIS Python API I developed my scripts in was at a magical time in history, when the API and Pandas worked together *perfectly*. Because of hardware changes and software updates, I found myself needing to replicate the Python environment. I hadn't exported the environment specs to a file, though… But how bad could it be?

Pretty bad. I tried a fresh install of the ArcGIS API in a brand new conda env, and it hit errors before it even finished *installing*. ArcGIS and Pandas refused to handle datetimes the same way, and gave conflicting error messages.

> Casting to unit-less 'datetime64' is not supported. Pass e.g. 'datetime64[ns]' instead.

Okay, sure.

> 'datetime64[ms]' is too specific of a frequency, try passing 'datetime64'

:/

We got things to limp along until the end of 2023, but it was clear that a change was going to be needed.


## Back to Basics

When it came to reworking things, I felt a little better this time around. I'd done enough non-Esri Python work that I was confident I could find a way around the ArcGIS Python API entirely. There had to be a simpler, leaner way of doing this stuff than installing Esri's gigantic module.

Here it is in a nutshell: ArcGIS Server has a REST endpoint, and most of what you do with it is JSON in a POST request. If you know how to get your data into the expected format, all you actually need is `requests`!

So, that's what I did, with plenty of GeoPandas thrown in for the spatial stuff. I put the functions into [into a git repo](https://codeberg.org/kendall-county-gis/geopython-functions) so that I could bring them into different projects, and so far it's working. I probably ought to learn how to put the project out as a legit Python package, but using git submodules is good enough for now.

The nightly scripts that use these functions are in their own repos ([https://codeberg.org/kendall-county-gis/fabric-updates](fabric-updates) and [https://codeberg.org/kendall-county-gis/devnet-extract](devnet-extract)), and are chugging along.
