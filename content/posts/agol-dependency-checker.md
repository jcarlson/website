---
title: AGOL / Enterprise Dependency Checker
description: Figure out where your layer is actually being used!
author: Josh Carlson
date: '2024-02-21'
toc: true
tags:
  - esri
  - python
aliases:
    - /blog/posts/agol-dependency-checker
---

## Overview

Once upon a time, I wrote and shared a Jupyter Notebook in ArcGIS Online for a common task: finding layer dependencies. Before I delete or overwrite a layer, I want to know: is *anything* actually using this layer? Is it safe to mess with?

I thought it was useful, so I shared the notebook publicly, but lately I've been getting messages that the notebook doesn't care how it's shared, it won't let you see it. So, let's bring it over here!

## The Process

In brief, this notebook will:

1. Take a given service's ItemID
1. Find the associated service URL
1. Iterate over all maps and apps in your org, looking for your service in
    1. Operational layers
    1. Basemap layers
    1. Anywhere else (search widget, etc.)

Matches will be printed out for user review.

## Setup

If you don't have a Python env available locally, using the AGOL notebooks is a great option. Even if you're an Enterprise user, you can connect to your Portal from AGOL.

```python
from arcgis.gis import GIS
import pandas as pd

gis = GIS("https://maps.co.kendall.il.us/portal")
```

### Define the ItemID / Service URL

Get the ItemID of your layer and input it here. Alternatively, you can just leave it empty and enter the service URL directly, since that's really what we need to do the search.

```python
find_id = input('ItemID: ') # c500c8284edd4112a9ee1a96236a72fc for demonstration

find_url = gis.content.get(find_id).url if find_id else input('Service URL: ')
```

## The Search

### Web Maps

First, we'll be getting a list of all the web maps in your org.

For each web map, it's as simple as using `get_data()` to return the JSON definition of the map as a string. We can then use `find` together with our service URL to see if it shows up.

#### Searching Outside Your Org

Obviously, the more maps and apps your org has, the longer this will take. If you include `outside_org=True` as a parameter in your search, though, be prepared for this to take a **very** long time to complete. 

```python
webmaps = gis.content.search('', item_type='Web Map', max_items=-1, outside_org=False)

map_list = [m for m in webmaps if str(m.get_data()).find(find_url) > -1]
```

### Web Apps

Now we'll be getting a list of all the web apps in your org. There are different `item_type`s for each, so we need to perform multiple queries and merge them together.

```python
apptypes = ['Application', 'Dashboard', 'Story Map', 'Web Experience']

webapps = [item for sublist in [gis.content.search('', item_type=t, max_items=-1, outside_org=False) for t in apptypes] for item in sublist]
```

Now that we have our apps, searching through them is a little different. A web map may reference your layer directly, using its URL **or** its ItemID. It may also simply reference the web map which contains your layer.

To search each of these, we create a list of `criteria`, then we use `any` to see if one or more of the criteria evaluate to `True`. When that is the case, we append that web app to a list of apps.

```python
app_list = []

for w in webapps:
    
    try:
        wdata = str(w.get_data())

        criteria = [
            wdata.find(find_url) > -1,
            wdata.find(find_id) > -1,
            any([wdata.find(m.id) > -1 for m in map_list])
        ]
        
        if any(criteria):
            app_list.append(w)
    
    # Some apps don't have data, so we'll just skip them if they throw a TypeError
    except:
        continue
```

# Review the Output

Now we've got our lists of maps and apps. But to get them in a user-friendly format, we'll use `pandas` to cobble together a dataframe.

```python
dependencies = pd.concat(
    [
        pd.DataFrame([{'title':a.title, 'id':a.id, 'type':a.type, 'url':f'{gis.url}/home/item.html?id={a.id}', 'data':a.get_data()} for a in app_list]),
        pd.DataFrame([{'title':m.title, 'id':m.id, 'type':m.type, 'url':f'{gis.url}/home/item.html?id={m.id}', 'data':m.get_data()} for m in map_list])
    ]
)

dependencies.reset_index(inplace=True, drop=True)

dependencies.head()
```

|title|id|type|url|data|
|-|-|-|-|-|
|Property Characteristics|d45e5b7e163140d797489dcf95f7f051|Web Mapping Application|https://maps.co.kendall.il.us/portal/home/item...|{'source': '417de4a1ec914ade845ae8bf746bc65f',...
|1.Voter Lookup|88c49889485b4815a28a8f799eba0343|Web Mapping Application|https://maps.co.kendall.il.us/portal/home/item...|{'source': 'b8248e713f354b2ab75c8b9cb43badff',...
|Elevation Compare|a80af3da6346497e93012a04c8ddc57b|Web Mapping Application|https://maps.co.kendall.il.us/portal/home/item...|{'source': 'df84c4cfd9674aa0ac1321c1c5c6338b',...
|Am I Incorporated?|8b09c733ed344a4181d89576015f6f56|Web Mapping Application|https://maps.co.kendall.il.us/portal/home/item...|{'source': '417de4a1ec914ade845ae8bf746bc65f',...
|Public Notification App|2b92d85a669f4c15a044790d0183d8b3|Web Mapping Application|https://maps.co.kendall.il.us/portal/home/item...|{'theme': {'name': 'JewelryBoxTheme', 'styles'...

## What Now?

Now you've got your output, but what do you do with it? Well, you could use `dependencies.to_csv()` to save the dataframe to a file. If you're just tracking down references to your service to replace or remove them, you can use the url column to go straight to the item's page in your portal and make the necessary adjustments.

### Extending

You could easily rework this script to be non-interactive, and instead supply a list of ItemIDs / URLs from a file. You still only need to get your map / app lists once, but could look for dependencies across any number of layers. But I'll leave that to you!