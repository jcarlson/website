---
title: Brioche Eyelet Scarf
author: "Josh Carlson"
date: "2023-12-12"
aliases:
    - /portfolio/brioche-eyelet-scarf
tags:
    - brioche
    - knitting
    - scarf
---

Thr first Christmas Gift of the season. Commission for a friend.

![](/media/crafts/brioche-eyelet-scarf1.jpg)

He picked the yarn himself, which can be a risky thing, but he went to a real yarn store and got some *nice* stuff, Malabrigo Rios. Really a pleasure to work with.

Based on a basic scroll lace motif, but this one in brioche knitting. You can see a regular version on [Stitch Maps](https://stitch-maps.com/patterns/display/scroll-lace-1/).

Been a while since I did brioche, so I had a couple failed starts figuring out the double-sided increases, but the end result was *great*. Sometime I'd like to see if I can get reversible single-sided increases in brioche that aren't eyelets...

It's actually a little hard to even see the eyelets at some angles, owing to how thick brioche knits up. But they're there.

![](/media/crafts/brioche-eyelet-scarf5.jpg)

![](/media/crafts/brioche-eyelet-scarf3.jpg)

![](/media/crafts/brioche-eyelet-scarf2.jpg)

![](/media/crafts/brioche-eyelet-scarf4.jpg)