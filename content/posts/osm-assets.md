---
title: 'ILGISA Meetup Recap / Assets in OpenStreetMap'
date: 2024-05-22T20:03:00-05:00
toc: true
tags:
    - OpenStreetMap
    - ILGISA
    - conference
---

## The Meetup

This past Monday was the [ILGISA](https://www.ilgisa.org/) Regional Meetup, a one-day conference for Illinois GIS Professionals in the northern Illinois area. It continued a positive trend of the past few years: more attendees, better content. Entirely worth the time, and if you're in the NE IL region, consider joining us next time. This year, there were so many great submissions that they cut the usual panel discussion! I'm not complaining.

### The Theme

The theme was "public works and utilities", and I went into it honestly expecting sales-y stuff. *"Here's our proprietary system for managing your assets. Don't worry, it's compatible with ArcMap 10.6!"* Not so!

Most of the presentations were simply individuals and organizations trying to tackle a problem. Some were contractors, sure, but none were *vendors*, really, and I think that makes a heck of a difference. The contractors are just trying to get the job done, same as me, and use whatever tool works. Still plenty of Esri-based work, of course. If there was a common thread, it was "here's how we used Experience Builder to do this". Ah well.

### The Topics

Within the stated theme, there was a good spread of topics. Mapping all-gender restrooms. Doing ADA sidewalk inventories for small towns. Planning for regional cycling connectivity.

It was hard to avoid the thought: "a lot of this could be done with OpenStreetMap".

## OSM Assets

Well, OSM is exactly what *my* presentation was all about! You can check out the slides [over here](https://slides.jcarlson.page/osm-assets/), but here's the quick summary.

Not every public works asset makes sense in OpenStreetMap. Underground water valves have trouble with the whole **verifiability** thing. But a *lot* of assets are exactly the kind of thing that people like to see in a map: benches, bathrooms, trails, trash cans.

What if an organization wanted to keep track of all that stuff, but they didn't want to stuff it into a data silo, or shell out for a proprietary system that they don't have the time or technical expertise to maintain? Or worse, hire a third-party contractor to maintain for them?

Well, OSM is kind of perfect, isn't it? You keep the history, the tagging is flexible, and if you don't wanna pay for it, you aren't obligated to. And there are some pretty good field apps!

Kendall wasn't really doing anything like that ourselves, but we thought about it a lot. Basically, all you need is the `check_date` field, maybe a `ref` tag, and some way of querying out the right data to review it later. A couple of OverPass queries, or the QuickOSM QGIS Plugin, something simple like that. So it started as a thought experiment, just to see what it would look and feel like.

I'm happy to say, as of last week we're officially going to manage our Forest Preserve's asset data in OSM, and *nowhere else*. It will be **the source** for trails, amenities, and the like, across all the County's preserves. First, though, we've gotta get out there and actually verify the data. Field trip!