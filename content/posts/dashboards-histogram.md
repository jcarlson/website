---
title: 'Making a Histogram in ArcGIS Dashboards'
description: It wasn't made for this, but that hasn't stopped me before.
date: 2024-04-28T04:02:49Z
draft: false
toc: true
tags:
    - ArcGIS Dashboards
    - Esri
    - Arcade
---

I like that more and more departments in the County want some kind of mapping / data support from GIS, but I do wish there weren't so many *dashboards*… Still, the ArcGIS Dashboards system is easy enough to work with. And thanks to **Data Expressions**, the right bit of Arcade can get around some of the system's constraints.

I love finding ways to mess with dashboards. Here's one that came up recently: histograms!

Dashboards have charts and numbers, but it's pretty basic. Histograms are not included. To get other kinds of charts, you have to get creative.

## Issues

A histogram needs *bins*, and those bins need to be labeled. *But*! The bins need to be in order, and they need to show up, *even when they're empty*. Even if you had a "bin number" field to group by, Arcade's serial chart widget will drop empty categories. No good.

![](/media/dashboards/histogram-emptybin.png)

And if you group by "bin number", *that* is the value shown on the category axis. You'd need to manually override the labels to show the bin ranges. Sort of defeats the purpose of trying to do it automatically.

![](/media/dashboards/histogram-labels.png)

If we group by the "bin label", a bin of `110 - 121` will sort between `11 - 22` and `22 - 33`. Also no good.

![](/media/dashboards/histogram-sorting.png)

## Workarounds

### Sorting

Rather than letting the *chart* group our values, we need to group them in the expression itself. This will return a single row per category, and we can then set our chart to follow *features*. In this way, the charted series and the sort order can be independent of one another.

### Empty Bins

I *really* wanted a solution that only used GroupBy with some SQL. It would be so *fast*! There may still be a way of doing it, but I kept running into the issue of empty bins being dropped from the output. I needed the bins to exist *first*, the get the feature counts put in later. A loop it is.

## Other Considerations

I could have hard-coded a solution to the immediate problem, but instead, I wanted a solution that could easily be used in other situations, with other data. I needed a custom function that could be a bit flexible, something that could

- Take any FeatureSet with a numeric field
- Adjust for the data range
- Let the user specify the bin count

## Pseudocode

1. Calculate the minimum and maximum values of the data
1. Determine from the range and bin count how large the range of a single bin should be
1. Create a series of empty bins with the appropriate labels
1. For each feature in the set:
    1. Determine its bin
    1. Increment the count in the corresponding bin
1. Return the histogram FeatureSet

```js
function Histogram(fset, field, bins){
    /*
    Generates a histogram from a FeatureSet
    
    Parameters:
        - fset: a FeatureSet
        - field: the field name with the numeric values to be binned
        - bins: the desired number of bins
    
    Returns:
        FeatureSet with one row per bin. Rows include a bin label, bin number, and count.
    */

    // get the upper and lower bounds of the data    
    var hmin = Min(fset, field)
    var hmax = Max(fset, field)
    
    // determine the size of each bin
    var step = Floor((hmax - hmin) / (bins - 1))
    
    // histogram featureset dictionary to hold values
    var hist_fs = {
        fields: [
            {name: 'bin_num', type: 'esriFieldTypeInteger'},
            {name: 'bin_name', type: 'esriFieldTypeString'},
            {name: 'the_count', type: 'esriFieldTypeInteger'}
        ],
        geometryType: '',
        features: [{attributes:{
            bin_num: 0,
            bin_name: `< ${hmin + step}`,
            the_count: 0
        }}]
    }
    
    // populate dictionary with empty bins
    var b_idx = 1
    
    for (var i = hmin + step; i < (hmax - step); i += step) {
        Push(
            hist_fs['features'],
            { attributes: {
                bin_num: b_idx,
                bin_name: `${i} — ${i + (step - 1)}`,
                the_count: 0
            }}
        )
        
        b_idx += 1
    }
    
    Push(
        hist_fs['features'],
        { attributes: {
            bin_num: b_idx,
            bin_name: `> ${i}`,
            the_count: 0
        }})
    
    // loop through input featureset and grab values
    for (var feat in fset) {
        // figure out which bin it falls into
        var bnum = Iif(
            feat['val'] == hmin,
            0,
            Ceil((feat['val'] - hmin)/step) - 1
        )
        
        // increment count on corresponding bin
        hist_fs['features'][bnum]['attributes']['the_count'] += 1
    }
    
    return FeatureSet(Text(hist_fs))
```

## Alternate Version: Filter and Count

An update!

Another user reached out to me about this post a while back. The above function works, but it's not very performant on a large layer. They suggested an alternate approach, which is probably an improvement upon the original regardless of the size of the layer.

```js
function Histogram(fset, field, bins) {
  var hmin = Min(fset, field)
  var hmax = Max(fset, field)
  var step = Floor((hmax - hmin) / (bins - 1))

  var hist_fs = {
    fields: [
      {name: 'bin_num', type: 'esriFieldTypeInteger'},
      {name: 'bin_name', type: 'esriFieldTypeString'},
      {name: 'the_count', type: 'esriFieldTypeInteger'}
    ],
    geometryType: '',
    features: [{
      attributes: {
        bin_num: 0,
        bin_name: `< ${Floor(hmin + step)}`,
        the_count: Count(Filter(fset, `${field} < ${hmin + step}`))
      }
    }]
  }

  var b_idx = 1
  
  for (var i = hmin + step; i < (hmax - step); i += step) {
    var the_count = Count(Filter(fset, `${field} > ${i - step} AND ${field} < ${i}`));
      Push(
          hist_fs['features'],
          { attributes: {
              bin_num: b_idx,
              bin_name: `${Floor(i)} — ${Floor(i + (step - 1))}`,
              the_count: the_count
          }}
      )
      
      b_idx += 1
  }

  Push(hist_fs['features'],
  { attributes: {
      bin_num: b_idx,
      bin_name: `> ${Floor(i)}`,
      the_count: Count(Filter(fset, `${field} > ${i}`))
    }
  })

  return FeatureSet(Text(hist_fs))
}
```

## Implementation

Once you drop the function into your expression, using it is as simple as:

```js
function Histogram(fset, field, bins){
    // function contents here
}

var fs = FeatureSetByPortalItem(
    Portal('your url'),
    'itemid',
    0,
    ['fieldA', 'fieldB'],
    false
)

return Histogram(fs, 'fieldA', 10)
```


![](/media/dashboards/histogram-charted.png)

![](/media/dashboards/histogram-bins.png)
