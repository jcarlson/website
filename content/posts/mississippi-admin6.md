---
title: 'Admin 6s from the Mississippi River'
description: 'Can I make that bad map worse?'
date: 2024-08-22T08:15:44-05:00
draft: false
toc: true
tags:
    - cartography
    - animation
    - maps
    - python
---

It started as a silly one-off, but grew from there.

## Inspiration

This map (not mine) was being posted around the internet, and a couple people forwarded it me:

![](/media/admin6_orig.jpg)
Image credit: Terrible Maps page on Facebook, so I'm told.

It's a fun "bad map".

- No real utility
- Insane color scheme
- Interesting to look at

## Let's make our own!

Pedantically, I thought about how Louisiana doesn't actually have "counties". But I wondered about Alaska. Surely Canada had *something* equivalent to a county. If these were included, could we get Alaska added to this?

Or really, let's just ignore national boundaries and work with OpenStreetMap's `admin_level=6` areas in any country. That should do it, right?

*It did!* Sort of.

![](/media/admin6.jpg)

### What's going on with Canada?

If there's one takeaway from this project, it's that I have *no idea* how the Canadian government is actually structured. Why are there such gaps in the `admin_level=6` areas?

(It appears some (but not all!) of these gaps are actually filled in by `admin_level=5`, which does nothing to clarify what's actually going on here for me.)

As it was put to me in a response to [my post on Mapstodon](https://mapstodon.space/@jcarlson/112995378348288160), OSM is probably correct, but it's "only as messy as the reality it's trying to reflect".

The map, though, says it's level 6 features from OSM, and that's accurate. We can worry about Canada later. Perhaps never.

### The color ramp

I was asked about the insane color ramp I picked. QGIS has a catalog of interesting color ramps from [cpt-city](http://seaviewsensing.com/pub/cpt-city/). Even in full-spectrum ramps, adjacent classes (there were 64) were too similar to really see the progression. But then I saw [colorcube](http://seaviewsensing.com/pub/cpt-city/h5/tn/colorcube.png.index.html), and I knew that was it.

## Some Python

The first time through, I was using QGIS's to:

1. Select features with a "distance from river" value that was `null`
2. Sub-select those features which touched a feature where the value was **not** `null`
3. Update the "distance from river" field to be whatever the next step was

It was incredibly tedious. And all for a bad map that nobody asked for, and which I was unlikely to recreate in the future.

Definitely worth my time to automate!

Note: the "origin" counties already have a `distance` value of `0` for this.

```py
import geopandas as gp

admin6 = gp.read_file('admin6.gpkg', layer='admin6')

d = 1
initn = 1
postn = 0

while initn != postn:

    initn = len(admin6.query('distance.isna()'))

    a0 = admin6.query('not distance.isna()').dissolve()
    xs = admin6.query('distance.isna()').geometry.intersects(a0.at[0, 'geometry'])
    idx = xs[xs == True].index
    admin6.loc[idx, 'distance'] = d
    
    postn = len(admin6.query('distance.isna()'))
    d += 1
```

Now that I have it, maybe I could recreate this using other features? Admin levels in other regions? Maybe with different rivers?

## Animate it!

I came back to this the following evening, and I had an irrepressible urge to *animate* the map. With some expression-based symbology and animation tools in **QGIS**, it was simple enough.

![](/media/mississippi_admin6.gif)

I thought it would be nice if this could seamlessly loop forever, like a good GIF. And so, I finally found a good use for the [Aneurism](http://seaviewsensing.com/pub/cpt-city/ggr/tn/Aneurism.png.index.html) color ramp!

![](/media/mississippi_admin6_pulse.gif)

