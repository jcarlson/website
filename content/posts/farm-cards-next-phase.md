---
title: "Farm Cards - The Next Phase"
description: "Third party vendors, I am coming for you"
author: "Josh Carlson"
date: "2023-11-11"
tags:
    - illinois
    - farmland
    - kendall county
aliases:
    - /blog/posts/farm-cards-next-phase
---

I mentioned that I came back from **FOSS4GNA** with a lot of ideas. One particularly grand idea: make my [farm cards script](/portfolio/farm-cards/) into a fully-fledged piece of software, and make it compatible with what the rest of the state. At various conferences, I have had the occasion to speak about this a number of times, and there does seem to be interest in developing and distributing this.

We wanted to pursue a partnership with the Illinois Department of Revenue. Provided we could secure some grant funding, would they be interested in the development of an *official* tool that we could just give to the counties?

The answer: *they would not*.

# Moving On

Well, who needs the state's blessing, anyway! Publication 122 is public, and I have demonstrated that this thing can actually work. It will take longer to go solo on this, and I've never actually created something with a GUI, but I'm up to the challenge.

I've also kept thinking about [Paul Ramsey's closing keynote](https://www.youtube.com/watch?v=1OfunxBysmg). As with many open source endeavors, I don't stand to make much (or even any) money from this. But if even a handful of counties decide to use it, the State of Illinois as a whole stands to benefit to the tune of thousands of dollars annually in software and licensing costs. That's worth pursuing, I think!

That's not to mention, those counties' farm assessments probably stand to improve in their quality and accuracy, too.

# A Gripe

I know plenty of folks who make their living contracting for public sector clients, and many of them are good people, so I've got to be careful not to use too broad a brush. But in local Illinois government, there seems to be a greater proportion of contractors, vendors, etc., who do **bad work**.

I have this gripe about a *lot* of things, but the farm cards assessment process seems to encapsulate it so well.

Many organizations (my own included), often buy a product or outsource entire tasks, and thus no longer feel it necessary to maintain or develop in-house expertise beyond simply interfacing with the software or vendor. The organization no longer has the capacity to recognize whether or not what they're getting is any good. But then, *good work* isn't the point so much as *someone else's work*. It ain't broke, so don't fix it!

## Reaction

I find this mentality intolerable, which is why we routinely reevaluate our procedures at Kendall County.

1. Are we utilizing the things we're paying for? Or for open-source, are we paying for the things we're utilizing?
2. Are the things we're paying for *worth the money*?
3. If we currently lack the expertise to answer #2, how can we address that?

# The Goal

Back on topic! If this particular task, assessing agricultural parcels, is such a problem for Illinois counties, what am I going to do about it? I'm going to **build the alternative**, and I'm going to ensure that **every county** can use it in their current system.

That's a big task. So, what do we need to make it happen? *Information.*

# The Survey

I'm going to attempt to get in touch with every county in the state and figure out how they're doing this currently. What tools are they using / paying for? What does their output need to look like? What do their inputs look like?

We'll see where it goes from there. Here's hoping that there aren't too many edge cases!
