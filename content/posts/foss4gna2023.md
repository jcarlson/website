---
title: "FOSS4GNA 2023"
author: "Josh Carlson"
date: "2023-10-27"
tags: [reflection, foss4g]
aliases:
    - /blog/posts/foss4gna2023
---

Earlier this week, I attended [FOSS4GNA](https://foss4gna.org/) for the first time, as well as [FedGeoDay](https://www.fedgeo.us/). I came home with a *lot* of ideas.

I also came home with the stark realization of how *isolated* I have been as a "GIS Developer". I have loved FOSS GIS tools for years, used a good number of them, and even gotten some of them officially adopted into our County workflows, with plans for more to come. With the exception of OpenStreetMap, however, I have not engaged with the broader community much.

My list of "things to look up" from this conference was immense. I am very much looking forward to testing some of these ideas out at Kendall County. What has been striking, though, is how many of these "new" ideas have been around for **years**. How could such amazing things happen in plain sight without my noticing?

Well, as I reticently admitted to many other conference-goers, we are still primarily an Esri shop over at the County. The set of "Esri customers" is immense, and there is a sense that you and your Esri peers *are* the GIS industry. It was certainly the case at last week's [ILGISA](https://www.ilgisa.org/) conference.

I felt like I was keeping up. I felt pretty knowledgeable and well informed. Heck, I'm on track to being in the top 3 "questions answered" folks on Esri's Community boards for the third year in a row! I know a *lot* about Esri's products.

But after this week, it's painfully clear how narrow my view was. Here was this incredible community of FOSS weirdos, ready and willing to share their work with anyone who wanted, working on amazing new ideas, and *I was completely missing it*.

I'm still riding the post-conference high, but I'm hoping that this becomes the catalyst for something bigger, that it doesn't just fade back to the weekly routines by next month. How do you sustain an intention? I think you just need to take *action*. So I'm going to build stuff, play around, all of that. But I'm also going to take that sense of *community* more seriously and engage with the kinds of people I was surrounded with in Baltimore.

Here is a sentence I never thought I'd write: I'm excited to sign up for some mailing lists.
