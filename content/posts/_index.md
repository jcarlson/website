---
title: Posts
description: Josh Carlson posting about things
draft: false
weight: 10
---

Work, crafts, code, etc. Small things that don't need to go in a [portfolio](/portfolio) item.