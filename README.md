# website

Files for building static website with Hugo.

`hugo deploy` will sync this to the bucket.

## Redeploy AWS site

On AWS Amplify, we have to tell it to redeploy with changes.

```
aws amplify start-deployment --app-id dsauiplu8l58h --branch-name production --source-url s3://jcarlson-website-content --source-url-type BUCKET_PREFIX
```